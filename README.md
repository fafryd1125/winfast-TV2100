# Leadtek WinFast TV2100 FM
An analog PCI tuner card from [Leadtek](http://www.leadtek.com/).

**Device is supported since Linux 4.4.**
Kernel patch can be downloaded from [linux-media tree][] (works with **4.2** and **4.3** kernel).

[linux-media tree]: http://git.linuxtv.org/cgit.cgi/media_tree.git/commit/?id=63ab664cebe5bbfd530d46ed7439e43aa4d45227

---
## Table of Contents
**[Overview/Features](#overviewfeatures)**
* [Components Used](#components-used)
* [Video Standards](#video-standards)
* [Connectors](#connectors)

**[Identification](#identification)**

**[Remote control support](#remote-control-support)**
* [Setup LIRC (recommended)](#setup-lirc-recommended)
* [IR remote control as regular input device](#ir-remote-control-as-regular-input-device)

**[Development Material](#development-material)**
* [RegSpy dumps](#regspy-dumps)
* [Useful Links](#useful-links)

**[External Links](#external-links)**

---

## Overview/Features
This card delivers one analogue channel.

### Components Used
* NXP/Philips SAA7130HL
* 32-bit PCI 2.3 bus mastering

### Video Standards
* NTSC, PAL or SECAM

### Connectors
* 1 for RF TV source reception
* 1 x phone jack for I/R sensor
* 1 x phone jack for audio input
* 1 x phone jack for audio output
* 1 x S-Video connector for video input
* 1 x Composite connector for video input
* 1 for FM source reception

## Identification
The card has a PCI subsystem ID of *107d:6f3a*. The output of `lspci -vvnn` should be similar to:

```shell
04:00.0 Multimedia controller [0480]: Philips Semiconductors SAA7130 Video Broadcast Decoder [1131:7130] (rev 01)
        Subsystem: LeadTek Research Inc. Device [107d:6f3a]
        Control: I/O- Mem+ BusMaster+ SpecCycle- MemWINV- VGASnoop- ParErr- Stepping- SERR- FastB2B- DisINTx-
        Status: Cap+ 66MHz- UDF- FastB2B+ ParErr- DEVSEL=medium >TAbort- <TAbort- <MAbort- >SERR- <PERR- INTx-
        Latency: 32 (21000ns min, 8000ns max)
        Interrupt: pin A routed to IRQ 18
        Region 0: Memory at ee100000 (32-bit, non-prefetchable) [size=1K]
        Capabilities: [40] Power Management version 1
                Flags: PMEClk- DSI- D1+ D2+ AuxCurrent=0mA PME(D0-,D1-,D2-,D3hot-,D3cold-)
                Status: D0 NoSoftRst- PME-Enable- DSel=0 DScale=1 PME-
        Kernel driver in use: saa7134
        Kernel modules: saa7134
```

Here is the output from `dmesg | grep saa7134`:

```shell
saa7134: saa7130/34: v4l2 driver version 0, 2, 17 loaded
saa7134 0000:04:00.0: enabling device (0000 -> 0002)
saa7134: saa7130[0]: found at 0000:04:00.0, rev: 1, irq: 18, latency: 32, mmio: 0xee100000
saa7134: saa7130[0]: subsystem: 107d:6f3a, board: Leadtek Winfast TV2100 FM [card=195,autodetected]
saa7134: saa7130[0]: board init: gpio is 6200c
input: saa7134 IR (Leadtek Winfast TV2 as /devices/pci0000:00/0000:00:1c.2/0000:03:00.0/0000:04:00.0/rc/rc0/input15
rc0: saa7134 IR (Leadtek Winfast TV2 as /devices/pci0000:00/0000:00:1c.2/0000:03:00.0/0000:04:00.0/rc/rc0
input: MCE IR Keyboard/Mouse (saa7134) as /devices/virtual/input/input17
rc rc0: lirc_dev: driver ir-lirc-codec (saa7134) registered at minor = 0
saa7134: i2c eeprom 00: 7d 10 3a 6f 54 20 1c 00 43 43 a9 1c 55 d2 b2 92
saa7134: i2c eeprom 10: 0c ff 82 0e ff 20 ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom 20: 01 40 02 03 03 02 01 03 08 ff 00 8c ff ff ff ff
saa7134: i2c eeprom 30: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom 40: 50 89 00 c2 00 00 02 30 02 ff ff ff ff ff ff ff
saa7134: i2c eeprom 50: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom 60: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom 70: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom 80: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom 90: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom a0: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom b0: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom c0: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom d0: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom e0: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: i2c eeprom f0: ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff
saa7134: saa7130[0]: registered device video0 [v4l2]
saa7134: saa7130[0]: registered device vbi0
saa7134: saa7130[0]: registered device radio0

```

## Remote control support
Winfast TV2100 FM is supplied with **Leadtek Y04G0051** remote control.

### Setup LIRC (recommended)
Since **0.9.4pre1**, lirc is shipped with devinput-based default setup ("works" out of the box).
Please see commit [74ff24][] for more details.

[74ff24]: http://sourceforge.net/p/lirc/git/ci/74ff2480c293be9fdb3efa0f1bbefe27b39fa4aa/

* Install lirc for your distro.
* Place [leadtek_Y04G0051.conf][1] in `/etc/lirc/lircd.conf.d/`
* Edit file `/etc/lirc/lirc_options.conf`

```shell
-driver          = default
-device          = /dev/lirc0
+driver          = devinput
+device          = /dev/input/by-path/pci-0000:04:00.0-event-ir  # example
```

Execute `ls -la /dev/input/by-path/`. Example:

```shell
total 0
rwxr-xr-x 2 root root 140 10-17 10:34 .
drwxr-xr-x 4 root root 500 10-17 10:34 ..
lrwxrwxrwx 1 root root   9 10-17 10:34 pci-0000:00:14.0-usb-0:14:1.0-event-mouse -> ../event1
lrwxrwxrwx 1 root root   9 10-17 10:34 pci-0000:00:14.0-usb-0:14:1.0-mouse -> ../mouse0
lrwxrwxrwx 1 root root  10 10-17 10:34 pci-0000:04:00.0-event-ir -> ../event16
lrwxrwxrwx 1 root root   9 10-17 10:34 platform-i8042-serio-0-event-kbd -> ../event0
lrwxrwxrwx 1 root root   9 10-17 10:34 platform-pcspkr-event-spkr -> ../event5
```
* Start lirc using your init system (systemd, openrc, upstart, etc.)
* Test the remote using `/usr/bin/irw`, which simply echos anything received by LIRC when users push buttons on the remote to stdout.
Example:

```shell
$ irw
0000000080010002 00 KEY_1 Y04G0051
0000000080010003 00 KEY_2 Y04G0051
0000000080010004 00 KEY_3 Y04G0051
0000000080010005 00 KEY_4 Y04G0051
0000000080010006 00 KEY_5 Y04G0051
0000000080010179 00 KEY_TV Y04G0051
0000000080010164 00 KEY_POWER2 Y04G0051
```
### IR remote control as regular input device

Recent Linux kernels have built-in support for IR remotes. Using that, pressing an up-arrow on the remote works the same way as pressing the up-arrow on a keyboard.

If this is all you want, then you're done. However, [LIRC](http://www.lirc.org/) offers more flexibility and functionality.

#### Available Scancodes

Execute `ir-keytable` without any arguments. Example:

```shell
Found /sys/class/rc/rc0/ (/dev/input/event5) with:
    Driver saa7134, table rc-leadtek-y04g0051
    Supported protocols: other lirc rc-5 jvc sony nec sanyo mce-kbd rc-6 sharp xmp 
    Enabled protocols: lirc nec
    Name: saa7134 IR (Leadtek Winfast TV2
    bus: 1, vendor/product: 107d:6f3a, version: 0x0001
    Repeat delay = 500 ms, repeat period = 125 ms
```

Execute `ir-keytable -r -d /dev/input/PATH` where PATH is what the previous command outputted (event5 in the example above).

## Development Material
Two Windows based utilities that can be handy in the process of determining what to program for GPIO values in device definitions are:
* [RegSpy](http://sourceforge.net/projects/deinterlace/files/Tools/), included with [DScaler](http://deinterlace.sourceforge.net/)
* [BtSpy](http://btwincap.sourceforge.net)


### RegSpy dumps

```
SAA7130 Card [0]:

Vendor ID:           0x1131
Device ID:           0x7130
Subsystem ID:        0x6f3a107d


7 states dumped

----------------------------------------------------------------------------------

SAA7130 Card - State 0:
SAA7134_GPIO_GPMODE:             80000009 * (10000000 00000000 00000000 00001001)
SAA7134_GPIO_GPSTATUS:           0606200c * (00000110 00000110 00100000 00001100)
SAA7134_ANALOG_IN_CTRL1:         c1         (11000001)
SAA7134_ANALOG_IO_SELECT:        3b *       (00111011)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)


Changes: State 0 -> State 1:
SAA7134_GPIO_GPMODE:             80000009 -> 8000000d  (-------- -------- -------- -----0--)
SAA7134_GPIO_GPSTATUS:           0606200c -> 00062000  (-----11- -------- -------- ----11--)
SAA7134_ANALOG_IO_SELECT:        3b       -> 00        (--111-11)

3 changes


----------------------------------------------------------------------------------

SAA7130 Card - State 1:
SAA7134_GPIO_GPMODE:             8000000d   (10000000 00000000 00000000 00001101)  (was: 80000009)
SAA7134_GPIO_GPSTATUS:           00062000 * (00000000 00000110 00100000 00000000)  (was: 0606200c)
SAA7134_ANALOG_IN_CTRL1:         c1 *       (11000001)
SAA7134_ANALOG_IO_SELECT:        00 *       (00000000)                             (was: 3b)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)


Changes: State 1 -> State 2:
SAA7134_GPIO_GPSTATUS:           00062000 -> 00062008  (-------- -------- -------- ----0---)
SAA7134_ANALOG_IN_CTRL1:         c1       -> 83        (-1----0-)
SAA7134_ANALOG_IO_SELECT:        00       -> 09        (----0--0)

3 changes


----------------------------------------------------------------------------------

SAA7130 Card - State 2:
SAA7134_GPIO_GPMODE:             8000000d   (10000000 00000000 00000000 00001101)
SAA7134_GPIO_GPSTATUS:           00062008   (00000000 00000110 00100000 00001000)  (was: 00062000)
SAA7134_ANALOG_IN_CTRL1:         83 *       (10000011)                             (was: c1)
SAA7134_ANALOG_IO_SELECT:        09         (00001001)                             (was: 00)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)


Changes: State 2 -> State 3:
SAA7134_ANALOG_IN_CTRL1:         83       -> c8        (-0--0-11)

1 changes


----------------------------------------------------------------------------------

SAA7130 Card - State 3:
SAA7134_GPIO_GPMODE:             8000000d   (10000000 00000000 00000000 00001101)
SAA7134_GPIO_GPSTATUS:           00062008 * (00000000 00000110 00100000 00001000)
SAA7134_ANALOG_IN_CTRL1:         c8 *       (11001000)                             (was: 83)
SAA7134_ANALOG_IO_SELECT:        09 *       (00001001)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)


Changes: State 3 -> State 4:
SAA7134_GPIO_GPSTATUS:           00062008 -> 04062004  (-----0-- -------- -------- ----10--)
SAA7134_ANALOG_IN_CTRL1:         c8       -> c1        (----1--0)                             (same as 0, 1)
SAA7134_ANALOG_IO_SELECT:        09       -> 00        (----1--1)                             (same as 1)

3 changes


----------------------------------------------------------------------------------

SAA7130 Card - State 4:
SAA7134_GPIO_GPMODE:             8000000d   (10000000 00000000 00000000 00001101)
SAA7134_GPIO_GPSTATUS:           04062004 * (00000100 00000110 00100000 00000100)  (was: 00062008)
SAA7134_ANALOG_IN_CTRL1:         c1         (11000001)                             (was: c8)
SAA7134_ANALOG_IO_SELECT:        00         (00000000)                             (was: 09)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)


Changes: State 4 -> State 5:
SAA7134_GPIO_GPSTATUS:           04062004 -> 02062000  (-----10- -------- -------- -----1--)

1 changes


----------------------------------------------------------------------------------

SAA7130 Card - State 5:
SAA7134_GPIO_GPMODE:             8000000d   (10000000 00000000 00000000 00001101)
SAA7134_GPIO_GPSTATUS:           02062000 * (00000010 00000110 00100000 00000000)  (was: 04062004)
SAA7134_ANALOG_IN_CTRL1:         c1         (11000001)
SAA7134_ANALOG_IO_SELECT:        00 *       (00000000)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)


Changes: State 5 -> Register Dump:
SAA7134_GPIO_GPSTATUS:           02062000 -> 06062008  (-----0-- -------- -------- ----0---)  
SAA7134_ANALOG_IO_SELECT:        00       -> 02        (------0-)

2 changes


=================================================================================

SAA7130 Card - Register Dump:
SAA7134_GPIO_GPMODE:             8000000d   (10000000 00000000 00000000 00001101)
SAA7134_GPIO_GPSTATUS:           06062008   (00000110 00000110 00100000 00001000)  (was: 02062000)
SAA7134_ANALOG_IN_CTRL1:         c1         (11000001)
SAA7134_ANALOG_IO_SELECT:        02         (00000010)                             (was: 00)
SAA7134_VIDEO_PORT_CTRL0:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL4:        00000000   (00000000 00000000 00000000 00000000)
SAA7134_VIDEO_PORT_CTRL8:        00         (00000000)
SAA7134_I2S_OUTPUT_SELECT:       00         (00000000)
SAA7134_I2S_OUTPUT_FORMAT:       01         (00000001)
SAA7134_I2S_OUTPUT_LEVEL:        00         (00000000)
SAA7134_I2S_AUDIO_OUTPUT:        01         (00000001)
SAA7134_TS_PARALLEL:             04         (00000100)
SAA7134_TS_PARALLEL_SERIAL:      00         (00000000)
SAA7134_TS_SERIAL0:              00         (00000000)
SAA7134_TS_SERIAL1:              00         (00000000)
SAA7134_TS_DMA0:                 00         (00000000)
SAA7134_TS_DMA1:                 00         (00000000)
SAA7134_TS_DMA2:                 00         (00000000)
SAA7134_SPECIAL_MODE:            01         (00000001)

end of dump
```

Here is the order in which I performed the test: State 6

State 0 - viewing software off

State 1 - tuner mode

State 2 - composite mode

State 3 - s video mode

State 4 - radio mode

State 5 - tuner mode (again)

Final dump - viewing software off (again)

### Useful Links
[Historical discussion - Leadtek Winfast TV2100](https://marc.info/?t=125770034200004&r=1&w=2)

[Remote controllers-V4L](https://linuxtv.org/wiki/index.php/Remote_controllers-V4L)

[Remote Controllers](https://linuxtv.org/wiki/index.php/Remote_Controllers)

[Add IR support for saa7134](https://linuxtv.org/wiki/index.php/Add_IR_support_for_saa7134)

[GPIO pins](https://linuxtv.org/wiki/index.php/GPIO_pins)

[Documentation/video4linux](https://linuxtv.org/downloads/v4l-dvb-apis-new)

## External Links
[Leadtek WinFast page](http://www.leadtek.com)

[1]: https://gitlab.com/fafryd1125/winfast-TV2100/blob/master/leadtek_Y04G0051.conf
